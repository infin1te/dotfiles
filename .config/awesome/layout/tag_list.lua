local awful = require("awful");
local wibox = require("wibox");
local gears = require("gears");

local taglist_buttons = gears.table.join(
    awful.button({ }, 1, function(t) t:view_only() end),
    awful.button({ modkey }, 1, 
        function(t)
            if client.focus then
                client.focus:move_to_tag(t)
            end
        end),
    awful.button({ }, 3, awful.tag.viewtoggle),
    awful.button({ modkey }, 3, 
        function(t)
            if client.focus then
                client.focus:toggle_tag(t)
            end
        end),
    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
)


local make_taglist = function(scr, taglist_width)

	taglist_width = taglist_width or 150;

	local taglist = awful.widget.taglist{
		screen = scr,
		filter = awful.widget.taglist.filter.all,
		layout = { spacing = 10, layout = wibox.layout.flex.horizontal },
        buttons = taglist_buttons,
		widget_template = {
			widget = wibox.container.background,
			shape = gears.shape.rectangle,
			id = "background_role",
			bg = "#FF333300",
			{
				widget = wibox.container.margin,
				left = 5,
				right = 5,
				top = 6,
				bottom = 6,
				{	            
				 	id     = 'icon_role',
                 	widget = wibox.widget.imagebox,
            	},
			}
		}
    }
    return taglist
end

return make_taglist


