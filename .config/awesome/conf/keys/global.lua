local awful = require("awful");
local gears = require("gears");
local naughty = require("naughty");
local beautiful = require("beautiful");

local hotkeys_popup = require("awful.hotkeys_popup").widget
local modkey = require("conf.keys.mod").modkey;

local output_path = '~/pictures/$(date +"%Y-%m-%d-%H-%M-%S").png';

local globalkeys = gears.table.join(

	--[[ AwesomeWM ]]--
	awful.key({modkey}, "s", hotkeys_popup.show_help, {description="show help", group="awesome"}),
	awful.key(
		{modkey, "Control"}, "r", 
		awesome.restart, 
		{description = "reload awesome", group = "awesome"}
	),
	awful.key(
		{modkey, "Shift"}, "q",
		awesome.quit,
		{description = "quit awesome", group = "awesome"}
	),

	--[[ Hardware ]]--
		awful.key(
		{}, "XF86AudioMute",
		function()
			awful.spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle");
			naughty.notify(
				{
					title = "PulseAudio",
					text = "Toggled mute",
					timeout = 0.5
				}
			);
		end,
		{description = "toggle mute", group = "hardware"}
	),

	awful.key(
		{}, "XF86AudioPlay",
		function()
			awful.spawn("playerctl play-pause");
			naughty.notify(
				{
					title = "playerctl",
					text = "toggle music player",
					timeout = 0.5
				}
			);
		end,
		{description = "toggle music player", group = "hardware"}
	),

	awful.key(
		{}, "XF86AudioStop",
		function()
			awful.spawn("playerctl play-pause");
			naughty.notify(
				{
					text = "silent mode toggle",
					timeout = 0.5
				}
			);
		end,
		{description = "toggle music / deafen discord", group = "hardware"}
	),

	awful.key(
		{}, "Print",
		function()
			awful.spawn.easy_async_with_shell('maim -s --hidecursor | xclip -selection clipboard -t image/png', function(stdout, stderr, exitreason, exitcode)
				naughty.notify(
					{
						title = "AwesomeWM",
						text = "Screenshot taken (selection)\n" .. stderr,
						timeout = 0.5
					}

				);


			end );
			stderr = "";
		end,
		{description = "take screenshot (selection)", group = "awesome"}
	),

	awful.key(
		{modkey}, "Print",
		function()
			awful.spawn.easy_async_with_shell("sleep 0.1 && maim " .. output_path, function(stdout)
				naughty.notify(
				{
					title = "AwesomeWM",
					text = "Screenshot taken (fullscreen)",
					timeout = 0.5
				}
				);
			end );
		end,
		{description = "take screenshot (fullscreen)", group = "awesome"}
	),

	--[[ Launcher ]]--
	awful.key(
		{modkey}, "r",
		function() awful.spawn.with_shell("PATH=$PATH:~/scripts rofi -show run") end,
		{description = "run", group = "launcher"}
	),
	awful.key(
		{modkey}, "w",
		function() awful.spawn("rofi -show window") end,
		{description = "show window list", group = "launcher"}
	),
	awful.key(
		{modkey}, "t",
		function() awful.spawn("kitty") end,
		{description = "open a terminal", group = "launcher"}
	),

	--[[ Tags ]]--
	awful.key(
		{modkey}, "Left",
		awful.tag.viewprev,
		{description = "view previous", group = "tag"}
	),
	awful.key(
		{modkey}, "Right",
		awful.tag.viewnext,
		{description = "view next", group = "tag"}
	),

	--[[ Focus ]]--
	awful.key(
		{modkey}, "j",
		function() awful.client.focus.byidx(1) end,
		{description = "focus next by index", group = "client"}
	),
	awful.key(
		{modkey}, "k",
		function() awful.client.focus.byidx(-1) end,
		{description = "focus previous by index", group = "client"}
	),
	awful.key(
		{modkey}, "u",
		awful.client.urgent.jumpto,
		{description = "focus urgent client", group = "client"}
	),

	--[[ Layout ]]--
	awful.key(
		{modkey, "Shift"}, "j", 
		function() awful.client.swap.byidx(1) end,
        {description = "swap with next client by index", group = "client"}
	),
    awful.key(
		{modkey, "Shift"}, "k", 
		function() awful.client.swap.byidx(-1) end,
        {description = "swap with previous client by index", group = "client"}
	),
    awful.key(
		{modkey}, "l",     
		function() awful.tag.incmwfact(0.05) end,
        {description = "increase master width factor", group = "layout"}
	),
    awful.key(
		{modkey}, "h",
		function() awful.tag.incmwfact(-0.05) end,
        {description = "decrease master width factor", group = "layout"}
	),
    awful.key(
		{modkey, "Shift"}, "h",
		function() awful.tag.incnmaster(1, nil, true) end,
        {description = "increase the number of master clients", group = "layout"}
	),
    awful.key(
		{modkey, "Shift"}, "l",     
		function() awful.tag.incnmaster(-1, nil, true) end,
        {description = "decrease the number of master clients", group = "layout"}
	),
    awful.key(
		{modkey, "Control"}, "h",     
		function() awful.tag.incncol(1, nil, true) end,
        {description = "increase the number of columns", group = "layout"}
	),
    awful.key(
		{modkey, "Control"}, "l",     
		function() awful.tag.incncol(-1, nil, true) end,
        {description = "decrease the number of columns", group = "layout"}
	),
    awful.key(
		{modkey}, "space", 
		function() awful.layout.inc(1) end,
        {description = "select next", group = "layout"}
	),
    awful.key(
		{modkey, "Shift"}, "space", 
		function() awful.layout.inc(-1) end,
        {description = "select previous", group = "layout"}
	)
);
 
--[[ Bind tags to keyboard numbers ]]--
for i = 1, 9 do
	globalkeys = gears.table.join(globalkeys,
		awful.key(
			{modkey}, "#" .. i + 9,
			function()
				local screen = awful.screen.focused();
				local tag = screen.tags[i];
				if tag then
					tag:view_only();
					-- Focus first client in tag
					local c = awful.client.getmaster() or tag.clients(tag)[1]
					if c then client.focus = c end;
				end;
			end,
			{description = "view tag #" .. i, group = "tag"}
		),
		awful.key(
			{modkey, "Shift"}, "#" .. i + 9,
			function()
				if client.focus then
					local tag = client.focus.screen.tags[i];
					if tag then
						client.focus:move_to_tag(tag);
					end;
				end;
			end,
			{description = "move focused client to tag #" .. i, group = "tag"}
		)
	)
end;

return globalkeys;

