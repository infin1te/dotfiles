local awful = require("awful");
local gears = require("gears");

local modkey = require("conf.keys.mod").modkey;

return gears.table.join(
	awful.key(
		{modkey}, "q", 
		function(c) c:kill() end,
        {description = "close", group = "client"}
	),
	awful.key(
		{modkey}, "f",
		function(c)
			c.fullscreen = not c.fullscreen
			c:raise();
		end,
		{description = "toggle fullscreen", group = "client"}
	),
    awful.key(
		{modkey, "Control"}, "space",  
		awful.client.floating.toggle                     ,
        {description = "toggle floating", group = "client"}
	),
    awful.key(
		{modkey, "Control"}, "Return", 
		function(c) c:swap(awful.client.getmaster()) end,
        {description = "move to master", group = "client"}
	),
	awful.key(
		{modkey, "Shift"}, "t",
		function(c)
			c.maximized = not c.maximized
		end,
		{description = "toggle maximize", group = "client"}
	)
);


