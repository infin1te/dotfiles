# dotfiles 

Dotfiles for my arch install, mostly here so I can quickly clone them and get set up, but feel free to do whatever you want with them.

# Dependencies
Arch packages:
```
awesome chaotic-aur/picom-git rofi kitty pulseeffects 
```
