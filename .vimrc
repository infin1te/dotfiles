filetype plugin on
filetype indent on

set ignorecase
set smartcase
set hlsearch
set incsearch

syntax enable

set smarttab

set shiftwidth=4
set tabstop=4

set ai
set si
set wrap
